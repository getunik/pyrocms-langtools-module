# Language Tools for PyroCMS

Provides tools and utilities to support multilingual PyroCMS websites

- version - 1.0.0
- Authors - Lukas Angerer - lukas.angerer@getunik.com
- Website - [getunik AG](http://www.getunik.com)

## What is it?

This module provides tools to simplify working with multilingual PyroCMS websites. It currently consists of the following features:

### Plugins
* configurable language redirection
* 'page keys' and language dependent linking according to those page keys

## Page Keys
The community version of PyroCMS doesn't really _support_ multilingual websites, but there are several ways to still do this. (probably) the easiest one is to create separate page trees for the different languages. There are however some drawbacks with this approach. One of the more annoying ones is the following scenario:

Assume you have a plugin that is used on a page for every language, where the plugin itself can handle all languages through language files. Now if you want to link to a specific page like the 'about' page you get in a tricky situation. Your plugin doesn't know (and shouldn't care) the URLs of all the language version of the about page.

The langtools solution to this problem is to introduce a new field on the default page type that is generally referred to as the **page key** (the default field name of this field is **uid**, but it can be configured in the langtools.php config file). The page key is a two part identifier of the form "_name_-_lang_" (e.g. "about_en", "about_de", ...). If all the equivalent language pages are configured with the appropriate page key, it becomes fairly trivial to link to the right page depending on the current language.

It is important to note, that:
1. Not all pages need to have a page key defined, only those you actually want to link to from a language agnostic context.
2. You have to define all the page keys manually; there is no way of knowing which pages should have the same basic page key.

## Installation

1. Just install the module.
2. Add a new text field to the default page type and make sure to set the field slug to `uid` (or reconfigure the langtools appropriately).

## Plugins

### redirect and languageRedirect
In case you didn't know that already: you cannot route the root "/" route - just doesn't work.

A common use case for a language redirect is the home page. Since you can only have one single home page, but your multilanguage setup probably has one for each language, you probably want to redirect to the language's home page.

Both redirect methods support the three basic parameters

* method: `refresh` or `location` (default)
* code: HTTP status code of the redirect. Can be `301` or `302` (default). Only has an effect for location method.
* mode:
    * `plain`: the target is interpreted as a regular URL and passed through the `site_url` function to generate the actual target URL.
    * `prefixed`: the target is interpreted as a relative URL and the current language is added as a prefix (e.g. 'some/page' => 'en/some/page') before being passed to the site URL function.
    * `pageid`: the target is the (numeric) ID of a page as seen in the page details view of the page tree.
    * `pagekey`: the target is the name part of a page key (in the example above that would be 'about').

In addition to the three standard parameters, the **redirect** plugin only has one additional parameter called `target` which defines the redirect target (who would have thought?).

If you need more flexibility, you can use the **languageRedirect** plugin, which allows you to specify a `default` target and the possibility to override that default for any language by simply adding a parameter with the language ID.

#### Examples
The examples all assume that the base URL for the PyroCMS install is `http://somehost.com/` and the current language is `en`.

```
{{ langtools:redirect target="/some/page" }}

=> http://somehost.com/some/page (unconditionally)
```

```
{{ langtools:redirect target="/some/page" mode="prefixed" }}

=> http://somehost.com/en/some/page
```

```
{{ langtools:redirect target="mypage" mode="pagekey" code="301" }}

=> http://somehost.com/en/location/of/mypage-with-some-slug (301 redirect)
```

```
{{ langtools:languageRedirect default="/some/page" fr="/fr/une/page" }}

=> http://somehost.com/en/some/page or if language were French, http://somehost.com/fr/une/page
```

```
{{ langtools:languageRedirect mode="pageid" default="12" fr="8" }}

=> http://somehost.com/en/page/with/the-id-12` or if language were French, `http://somehost.com/fr/page/with/the-id-8
```

### pageUrl
The page URL plugin simply generates the URL of the page specified by the name part of it's `key` parameter.

```
{{ langtools:pageUrl key="mypage" }}
```

will return the URL `http://somehost.com/en/path/to/mypage`.
