<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Langtools extends Module
{
    public $version = '1.0.0';

    private $module_name;

    public function __construct()
    {
        parent::__construct();
        $this->module_name = strtolower(str_replace('Module_', '', get_class()));
    }

    public function info()
    {
		$config = array(
            'name' => array(
                'en' => 'Language Tools'
            ),
            'description' => array(
                'en' => 'Language Tools module to support multilingual websites'
            ),
            'backend' => false,
            'frontend' => false,
        );

        return $config;
    }

    public function install()
    {
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function uninstall()
    {
        return true;
    }
}
