<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Environment Plugin
 *
 * Simple plugin to render environment information
 *
 * @author		Lukas Anger
 * @copyright	Copyright (c) 2013, Lukas Angerer
 */
class Plugin_Langtools extends Plugin
{
	public $version = '1.0.0';
	private $langtoolsConfig;

	public $name = array(
		'en'	=> 'Language Tools'
	);

	public $description = array(
		'en'	=> 'Plugin functions to work with multilingual sites'
	);

	/**
	 * Returns a PluginDoc array that PyroCMS uses
	 * to build the reference in the admin panel
	 *
	 * All options are listed here but refer
	 * to the Blog plugin for a larger example
	 *
	 * @return array
	 */
	public function _self_doc()
	{
		$info = array(
			'redirect' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Redirects to the defined target'
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => '',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'target' => array(
						'type' => 'string',
						'flags' => '',
						'default' => '',
						'required' => true,
					),
					'method' => array(
						'type' => 'string',
						'flags' => '',
						'default' => 'location',
						'required' => false,
					),
					'code' => array(
						'type' => 'number',
						'flags' => '',
						'default' => '302',
						'required' => false,
					),
					'mode' => array(
						'type' => 'string',
						'flags' => '',
						'default' => 'plain',
						'required' => false,
					),
				),
			),
			'languageRedirect' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Performs a language specific redirect'
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => '',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'default' => array(
						'type' => 'string',
						'flags' => '',
						'default' => '',
						'required' => true,
					),
					'method' => array(
						'type' => 'string',
						'flags' => '',
						'default' => 'location',
						'required' => false,
					),
					'code' => array(
						'type' => 'number',
						'flags' => '',
						'default' => '302',
						'required' => false,
					),
					'mode' => array(
						'type' => 'string',
						'flags' => '',
						'default' => 'plain',
						'required' => false,
					),
				),
			),
			'pageUrl' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Gets the URL of a multilingual page by its page key'
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => '',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'key' => array(
						'type' => 'string',
						'flags' => '',
						'default' => '',
						'required' => true,
					),
				),
			),
			'variable' => array(
				'description' => array(// a single sentence to explain the purpose of this method
					'en' => 'Gets a variable (from the variables module) by combining the variable name with the context language'
				),
				'single' => true,// will it work as a single tag?
				'double' => false,// how about as a double tag?
				'variables' => '',// list all variables available inside the double tag. Separate them|like|this
				'attributes' => array(
					'name' => array(
						'type' => 'string',
						'flags' => '',
						'default' => '',
						'required' => true,
					),
					'mode' => array(
						'type' => 'string',
						'flags' => '',
						'default' => '',
						'required' => false,
					),
				),
			),
		);

		return $info;
	}

	public function __construct()
	{
		$this->config->load('langtools/langtools');
		$this->langtoolsConfig = $this->config->item('langtools');
	}

	/**
	 * redirect
	 *
	 * Usage:
	 * {{ langtools:redirect target="/some/page" }}
	 * {{ langtools:redirect target="mypage" mode="pagekey" code="301" }}
	 *
	 * @return void
	 */
	public function redirect()
	{
		$target = $this->attribute('target');
		$method = $this->attribute('method', 'location');
		$code = $this->attribute('code', 302);
		$mode = $this->attribute('mode', 'plain');

		$this->doRedirect($mode, $target, $method, $code);
	}

	/**
	 * languageRedirect
	 *
	 * Usage:
	 * {{ langtools:languageRedirect default="/some/page" fr="/fr/une/page" }}
	 * {{ langtools:languageRedirect mode="pageid" default="12" fr="8" }}
	 *
	 * @return void
	 */
	public function languageRedirect()
	{
		$method = $this->attribute('method', 'location');
		$code = $this->attribute('code', 302);
		$mode = $this->attribute('mode', 'plain');

		$target = $this->attribute(CURRENT_LANGUAGE);
		if ($target === NULL)
		{
			$target = $this->attribute('default');
			if ($target === NULL)
				return;
		}

		$this->doRedirect($mode, $target, $method, $code);
	}

	/**
	 * pageUrl
	 *
	 * Usage:
	 * {{ langtools:pageUrl key="mypage" }}
	 *
	 * @return string - the site_url of the page identified by the given page key
	 */
	public function pageUrl()
	{
		$page = $this->getPageByKey($this->attribute('key'));

		return site_url($page ? $page->uri : '');
	}

	public function variable()
	{
		$this->load->library('variables/variables');

		$name = $this->attribute('name');
		$mode = $this->attribute('mode', 'suffix');

		switch ($mode)
		{
			case 'prefix':
				$name = CURRENT_LANGUAGE . '_' . $name;
				break;

			default: // 'suffix'
				$name = $name . '_' . CURRENT_LANGUAGE;
				break;
		}

		return $this->variables->$name;
	}

	////////////////////////
	// Helpers and Internals

	private function getPageByKey($key)
	{
		$key = $key . '_' . CURRENT_LANGUAGE;
		$page = $this->db->where('def_page_fields.' . $this->langtoolsConfig['pageKeyField'], $key)
			->get('def_page_fields')
			->row_array();

		return $this->pyrocache->model('page_m', 'get', array($page['id'], false));
	}

	private function doRedirect($mode, $target, $method, $code)
	{
		switch ($mode)
		{
			case 'pageid':
				$page = $this->pyrocache->model('page_m', 'get', array($target, false));
				if (!is_object($page))
					return;
				$targetUrl = site_url($page ? $page->uri : '');
				break;

			case 'pagekey':
				$page = $this->getPageByKey($target);
				if (!is_object($page))
					return;
				$targetUrl = site_url($page ? $page->uri : '');
				break;

			case 'prefixed':
				$targetUrl = site_url("/{CURRENT_LANGUAGE}" . $target);

			default:
				$targetUrl = site_url($target);
				break;
		}

		redirect($targetUrl, $method, $code);
	}
}
